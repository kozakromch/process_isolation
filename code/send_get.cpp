#include "send_get.h"


Transmitter::Transmitter(std::string search_process) {$
	this->signal_number = 0;
	this->search_process = search_process;
	this->pid = _getpid();
}

Transmitter::~Transmitter(){$
	this->TerminateTransmitterThreads();
}

std::string Transmitter::GetString() {$
	std::string command = "";
	char let;
	std::bitset <BITSINLETTER> bitletter;
	int buf = 0;
	while (buf != STRING) {
		Sleep(TIMETOSLEEP);
		buf = this->GetObj();
	}
	
	buf = this->GetObj();
	
	do {
		bitletter.reset();			
		if (buf != LETTER) {
			throw(TransmitterException("Error in Prototocol LETTER first"));
		}
		for(int i = 0; i < BITSINLETTER; i++) {
			if (this->GetObj() != BIT) {
				throw(TransmitterException("Error in Prototocol BIT first"));
			}
			bitletter[i] = this->GetObj();
		}
		let = (char)bitletter.to_ulong();
		command += let;
		std::cout << command;
		buf = this->GetObj();
	} while (buf != STRING);

	return command;
}

int Transmitter::GetObj() {$
	int obj = this->GetSignatureFromProcess() - 1;
	while(obj == this->last_getted_letter){
		Sleep(TIMETOSLEEP);
		obj = this->GetSignatureFromProcess() - 1;
	}
	this->last_getted_letter = obj;
	this->SetSignature(obj);
	return obj;
}

void Transmitter::WaitForProcess(){$
	while (this->GetSignatureFromProcess() <= 0) {
		Sleep(TIMETOSLEEP*100);
		std::cout<<"WaitForProcess";
	}
}

int Transmitter::GetSignatureFromProcess(){$
	HANDLE hProcessSnap;
	HANDLE hProcess;
	PROCESSENTRY32 pe32;
	DWORD dwPriorityClass;
	int number_of_threads = 0;

	hProcessSnap = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0);
	if( hProcessSnap == INVALID_HANDLE_VALUE) {
		throw(TransmitterException("INVALID_HANDLE_VALUE"));
	}

	pe32.dwSize = sizeof( PROCESSENTRY32 );

	if ( !Process32First( hProcessSnap, &pe32)) {
		CloseHandle( hProcessSnap );
		throw("No Process");
	}

	do {
		std::string process_name = pe32.szExeFile;
		
		if ((this->search_process == process_name) && (this->pid != pe32.th32ProcessID)) {
			number_of_threads = pe32.cntThreads;
			break;
		}
	} while(Process32Next(hProcessSnap, &pe32));

	CloseHandle( hProcessSnap );
	return(number_of_threads);
}

void Transmitter::SetSignature(int signal_number) {$
	if (this->signal_number > 0) {
		this->TerminateTransmitterThreads();
	}
	this->signal_number = signal_number;
	for(int i=0; i < signal_number; i++ )
	{
		(this->hThreadArray)[i] = CreateThread(NULL, 0, ThreadFunction, NULL, 0, &(this->dwThreadIdArray)[i]);
		if ((this->hThreadArray)[i] == NULL) 
		{
			throw TransmitterException("Error_create thread");
		}
	}
}

void Transmitter::TerminateTransmitterThreads () {$
	ALL_THREADS_EXIT_FLAG = true;

    DWORD dwExit[MAX_THREADS];
    for(int i = 0; i < this->signal_number; i++)
    {
        WaitForSingleObject(this->hThreadArray[i], INFINITE);

        // get the thread exit code (I'm not sure why you need it)
        GetExitCodeThread(hThreadArray[i], &dwExit[i]);

        // cleanup the thread
        CloseHandle(this->hThreadArray[i]);
        hThreadArray[i] = NULL;
    }
    ALL_THREADS_EXIT_FLAG = FALSE;
}

bool Transmitter::Check (int supposed_signal_number) {$
	if ((this->GetSignatureFromProcess() - 1) == supposed_signal_number) {
		return TRUE;
	}
	return FALSE;
}

void Transmitter::SendCheck (int sign) {$
	std::cout << "sign = " << sign << std::endl;
	this->SetSignature(sign);
	std::cout <<"realsignature = " << GetSignatureFromProcess() << std::endl;
	int i = 0;
	while(!(this->Check(sign))) {
		Sleep(TIMETOSLEEP);
		if (i >= MAXPERIODS) {
			throw(TransmitterException("To much time"));
		}
		i++;
	}
}

void Transmitter::SendLetter(char letter) {$
	for(int i = 0; i < BITSINLETTER; i++)
	{
		int bit = (letter >> i) & 1;
		this->SendCheck(BIT);
		this->SendCheck(bit);
	}
}

void Transmitter::SendString(std::string send_string) {$
	for(auto i = send_string.begin(); i != send_string.end(); i++) {
		this->SendCheck(LETTER);
		this->SendLetter(*i);
	}
}

void Transmitter::SendCommand(std::string command) {$
	WaitForProcess();
	this->SendCheck(STRING);
	this->SendString(command);
	this->SendCheck(STRING);
}

DWORD WINAPI ThreadFunction(LPVOID lpParam) {$ 
	while(!ALL_THREADS_EXIT_FLAG){
		Sleep(50);
	}

	return 0; 
}

int main () {
	try{
		$
		Transmitter examp("receive.exe");
		std::string command;
		std::cin >> command;
		examp.SendCommand(command);
		return 0;
	}
	catch(TransmitterException exp){
		std::cout << exp.what();
	}
}
