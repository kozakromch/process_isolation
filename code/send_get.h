#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
#include <iostream>
#include <string>
#include <tlhelp32.h>
#include <exception>
#include <process.h>
#include <bitset>

#define BIT 2
#define LETTER 3
#define STRING 4

#define TIMETOSLEEP 10
#define MAXPERIODS 100
#define BITSINLETTER 8
#define MAX_THREADS 4

bool ALL_THREADS_EXIT_FLAG = FALSE;

DWORD WINAPI ThreadFunction(LPVOID lpParam);

class TransmitterException: public std::exception {
private:
	std::string m_error;
public:
	TransmitterException(std::string error)
	: m_error(error){}
	const char* what() const noexcept { 
		return m_error.c_str(); 
	}
};

class Transmitter {
private:
	DWORD   dwThreadIdArray[MAX_THREADS];
	HANDLE  hThreadArray[MAX_THREADS]; 
	std::string search_process = "";
	int signal_number;
	int pid;
	int last_getted_letter = -1;

	int GetSignatureFromProcess();
	void SetSignature(int signal_number);
	bool Check (int supposed_signal_number);
	void SendCheck (int sign);
	void SendLetter(char letter);
	void SendString(std::string send_string);
	void WaitForProcess();
	void TerminateTransmitterThreads();
public:
	Transmitter(std::string search_process);
	~Transmitter();
	void SendCommand(std::string send_command);
	std::string GetString();
	//char GetLetter();
	int GetObj();
};


    
#include <iostream>
#include <cassert>
#include <new>
#include <ctime>
#include <fstream>
#include <cstring>

using namespace std;

#define MAXLENGTH 100
#define $ Cmonitor log__(__FILE__, __LINE__, __FUNCTION__);

using namespace std;

class Cmonitor
{
private:
	char cfile_[MAXLENGTH];
	char cfunc_[MAXLENGTH];
	unsigned int actual_time;
	static int indent_level;
	int cline_;
	static ofstream log_file;
	int pid_;
public:
	Cmonitor(const char*, int, const char*);
	~Cmonitor();
};
int Cmonitor::indent_level;
ofstream Cmonitor::log_file;
Cmonitor::Cmonitor(const char* file, int line, const char* func)
{
	pid_ = _getpid();
	strcpy(cfile_, "");
	strcpy(cfunc_, func);
	cline_ = line;
	actual_time = clock();
	if (!log_file.is_open())
	{
		log_file.open("LOG" + to_string(pid_) + ".txt", ios_base::out | ios_base::trunc);
	}
	for (int i = 0; i < indent_level; i++)
	{
		log_file << "|	|";
	}
	indent_level++;
	log_file << " time: " << (clock() - actual_time)\
		<< " file: " << cfile_ << " line: " << cline_\
		<< " function: " << cfunc_ << " open " << endl;
}
Cmonitor::~Cmonitor()
{
	indent_level--;
	for (int i = 0; i < indent_level; i++)
	{
		log_file << "|	|";
	}
	log_file << " time: " << (clock() - actual_time)\
		<< " file: " << cfile_ << " line: " << cline_ \
		<< " function: " << cfunc_ << " close " << endl;
}
