#include <windows.h>
#include <tlhelp32.h>
#include <tchar.h>
#include <stdio.h>
#include <iostream>
#include <string>

#define DEBUG 1
#pragma comment(lib, "Advapi32.lib")
#pragma comment(lib, "ntdll.lib")

BOOL set_privileges(LPCTSTR szPrivName) {
	TOKEN_PRIVILEGES token_priv = { 0 };
	HANDLE hToken = 0;
	token_priv.PrivilegeCount = 1;
	token_priv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	if (!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &hToken))
	{

		std::cout << "OpenProcessToken error: " << GetLastError() <<std::endl;

		return FALSE;
	}
	if (!LookupPrivilegeValue(NULL, szPrivName, &token_priv.Privileges[0].Luid))
	{

		std::cout << "LookupPrivilegeValue error: " << GetLastError()<< std::endl;

		CloseHandle(hToken);
		return FALSE;
	}
	if (!AdjustTokenPrivileges(hToken, FALSE, &token_priv, sizeof(token_priv), NULL, NULL))
	{

		std::cout << "AdjustTokenPrivileges error: " << GetLastError() << std::endl;

		CloseHandle(hToken);
		return FALSE;
	}
	return TRUE;
}

DWORD get_pid_from_name(IN const char * pProcName) {
	HANDLE snapshot_proc = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (snapshot_proc == INVALID_HANDLE_VALUE)
	{

		std::cout << "CreateToolhelp32Snapshot error: " << GetLastError() << std::endl;

		return 0;
	}
	PROCESSENTRY32 ProcessEntry;
	DWORD pid;
	ProcessEntry.dwSize = sizeof(ProcessEntry);
	if (Process32First(snapshot_proc, &ProcessEntry))
	{
		while (Process32Next(snapshot_proc, &ProcessEntry))
		{
			if (!stricmp(ProcessEntry.szExeFile, pProcName))
			{
				pid = ProcessEntry.th32ProcessID;
				CloseHandle(snapshot_proc);
				return pid;
			}
		}
	}
	CloseHandle(snapshot_proc);
	return 0;
}

HANDLE get_process_handle(IN DWORD pid, DWORD access) {
	HANDLE hProcess = OpenProcess(access, FALSE, pid);
	if (!hProcess)
	{

		std::cout << "OpenProcess error: " << GetLastError() << std::endl;

		return FALSE;
	}
	return hProcess;
}

BOOL kill_proc1(IN DWORD pid) {
	 std::cout <<"TRY 1";
	 HANDLE hProc = get_process_handle(pid, PROCESS_TERMINATE); //Обрати внимание на режим доступа — мы не просим ничего лишнего
	 if (!TerminateProcess(hProc, 0))
	 {

	 	std::cout << "TerminateProcess error: " << GetLastError() << std::endl;

	 	return FALSE;
	 }
	 return TRUE;
}

BOOL kill_proc2(IN DWORD pid) {
	 std::cout <<"TRY 2";
		
		HANDLE hProc = get_process_handle(pid, PROCESS_CREATE_THREAD |PROCESS_VM_OPERATION);
		HMODULE hKernel32 = GetModuleHandle("kernel32.dll");
		if (!hKernel32)
			return FALSE;
		void *pExitProcess = GetProcAddress(hKernel32, "ExitProcess");
		if (!pExitProcess)
			return FALSE;
		HANDLE hThread = CreateRemoteThread(hProc, NULL, 0, (LPTHREAD_START_ROUTINE)pExitProcess, NULL, 0, NULL);
		if (!hThread)
		{

			std::cout << "CreateRemoteThread error: " << GetLastError() << std::endl;

			return FALSE;
		}
		return TRUE;
}

BOOL kill_proc3(IN DWORD pid) {
	 std::cout <<"TRY 3";
   
   HANDLE hProc = get_process_handle(pid, PROCESS_SET_QUOTA | PROCESS_TERMINATE);
   HANDLE job = CreateJobObjectA(NULL, NULL);
   if (!job)
   {
#ifdef DEBUG
       std::cout << "CreateJobObjectA error: " << GetLastError() <<std::endl;
#endif
       return FALSE;
   }
   if (!AssignProcessToJobObject(job, hProc))
   {
#ifdef DEBUG
       std::cout << "AssignProcessToJobObject error: " << GetLastError() << std::endl;
#endif
       return FALSE;
   }
   if (!TerminateJobObject(job, 0))
   {
#ifdef DEBUG
       std::cout << "TerminateJobObject error: " << GetLastError() << std::endl;
#endif
       return FALSE;
   }
   return TRUE;
}

BOOL kill_proc4(IN DWORD pid)
{
	 std::cout <<"TRY 4";

   HANDLE hProc = get_process_handle(pid, PROCESS_SUSPEND_RESUME);
   HANDLE dbg_obj = NULL;
  // NTSTATUS status = CreateDebugObject(&dbg_obj, 0x2, NULL, 0x1);
   NTSTATUS status = DebugActiveProcess(pid);
   CloseHandle(hProc);
   return FALSE;
}

BOOL kill_proc5(IN DWORD pid)
   {
	 std::cout <<"TRY 5";

       HANDLE hProc = get_process_handle(pid, PROCESS_VM_OPERATION);
       unsigned int count = 0;
       size_t sz = 0x400000000;    // 16 Гбайт
       while (sz >= 0x1000)
       {
           void *mem = VirtualAllocEx(hProc, NULL, sz, MEM_RESERVE, PAGE_READONLY);
           if (mem) count++;   //
           else sz /= 2;       // Будем занимать память допоследнего
       }
       CloseHandle(hProc);
       return FALSE;
   }

BOOL kill_proc6(IN DWORD pid)
{
	 std::cout <<"TRY 6";

   HANDLE hProc = get_process_handle(pid, PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | SYNCHRONIZE);
   char* address = NULL;
   while (address < (char*)0x80000000000)
   {
       MEMORY_BASIC_INFORMATION mem_bi;
       DWORD mem = VirtualQueryEx(hProc, address,&mem_bi, sizeof(mem_bi));
       if (mem)
       {
           if (mem_bi.State == MEM_COMMIT)
           {
               DWORD protect_state;
               VirtualProtectEx(hProc,
                       mem_bi.BaseAddress,
                       mem_bi.RegionSize,
                       PAGE_NOACCESS,
                       &protect_state);
           }
           address = (char*)((char*)mem_bi.BaseAddress + mem_bi.RegionSize);
       }
       else break;
   }
   CloseHandle(hProc);
   return FALSE;
}

BOOL kill_proc7(IN DWORD pid)
{
	 std::cout <<"TRY 7";

   HANDLE hProc = get_process_handle(pid, PROCESS_DUP_HANDLE);
   int i = 0;
   while ( i < 0x10000 )
   {
       HANDLE hndl = (HANDLE)i;
       HANDLE dublicate_h = NULL;
       if (DuplicateHandle(hProc, hndl, GetCurrentProcess(), &dublicate_h, 0, FALSE, DUPLICATE_CLOSE_SOURCE))
       {
           i++;
           CloseHandle(dublicate_h);
       }
   }
   CloseHandle(hProc);
   return TRUE;
}

BOOL kill_threads1(IN DWORD tid)
{
   HANDLE hTread = OpenThread(THREAD_SET_CONTEXT, FALSE, tid);
   HMODULE hKernel32 = GetModuleHandle("kernel32.dll");
   if (!hKernel32)
       return FALSE;
   void *pExitProcess = GetProcAddress(hKernel32, "ExitProcess");
   if (!pExitProcess)
       return FALSE;
   if (!QueueUserAPC((PAPCFUNC)pExitProcess, hTread, 0))
   {
#ifdef DEBUG
       std::cout << "QueueUserAPC error: " << GetLastError() << std::endl;
#endif
       return FALSE;
   }
   return TRUE;
}

BOOL kill_threads2(IN DWORD tid)
{
   HANDLE hThread = OpenThread(THREAD_TERMINATE,
       FALSE,
       tid);
   if (!TerminateThread(hThread, 0))
   {
#ifdef DEBUG
       std::cout << "TerminateThread error: " << GetLastError() <<std::endl;
#endif
       return FALSE;
   }
   return TRUE;
}

BOOL kill_threads3(IN DWORD tid)
{
   HANDLE hThread = OpenThread(THREAD_SET_CONTEXT,
                       FALSE,
tid);
   CONTEXT ctx;
   memset(&ctx, 0, sizeof(ctx));       // Выделяем память ctx изаполняем ее нулями
   ctx.ContextFlags = CONTEXT_CONTROL;
   SetThreadContext(hThread, &ctx);    // Меняем контекст
   CloseHandle(hThread);
   return TRUE;
}

BOOL get_threads(IN const char * pProcName)
{
   // Для получения списка потоков мы используем ту же функцию, что и для получения
   // списка процессов, только передаем ей параметр TH32CS_SNAPTHREAD
   HANDLE pTHandle = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
   ULONG process_tid[256];
   int tid_count = 0;
   int number_of_threads = 0;
   THREADENTRY32 ThreadEntry;
   ThreadEntry.dwSize = sizeof(ThreadEntry);
   DWORD pid = get_pid_from_name(pProcName);
   // Используем похожие функции для потоков, как и в случае с процессами
   if (Thread32First(pTHandle, &ThreadEntry))
   {
       do{
           if (ThreadEntry.dwSize >= FIELD_OFFSET(THREADENTRY32, th32OwnerProcessID) + sizeof(ThreadEntry.th32OwnerProcessID)) {
               // Здесь определяем потоки для нужного нам процесса
               if (ThreadEntry.th32OwnerProcessID == pid)
               {
                   process_tid[tid_count] = ThreadEntry.th32ThreadID;
#ifdef DEBUG
                   std::cout << "PID: " << pid << " " << "ThreadID:" << process_tid[tid_count] << std::endl;
#endif
                   tid_count = tid_count + 1;
                   ++number_of_threads;
               }
           }
           ThreadEntry.dwSize = sizeof(ThreadEntry);
       } while (Thread32Next(pTHandle, &ThreadEntry));
#ifdef DEBUG
       std::cout << "Number Threads: " << number_of_threads << std::endl;
#endif
       // Процесс один, а потоков несколько. Поэтому используемцикл, чтобы обойти их все
       for (int i = number_of_threads; i >= 0; --i)
       {
           kill_threads1(process_tid[number_of_threads]);   // В этом цикле мы будем помещать функции убийства потоков
           kill_threads2(process_tid[number_of_threads]);
           kill_threads3(process_tid[number_of_threads]);  
#ifdef DEBUG
           std::cout << "Thread kill: " << number_of_threads << std::endl;
#endif
       }
   }
   return TRUE;
}


int main () {
		if (set_privileges(SE_DEBUG_NAME)) {
			std::cout << "SE_DEBUG_NAME is granted! \n";
		}
		std::string process_name;
		std::cin >> process_name;
		DWORD notepad_pid = get_pid_from_name(process_name.c_str());
		get_threads(process_name.c_str());
	HANDLE hNotepad = get_process_handle(notepad_pid, PROCESS_ALL_ACCESS);
		if (kill_proc1(notepad_pid)) {
			std::cout << process_name << " killed 1" << std::endl;
		} else if (kill_proc2(notepad_pid)){
			std::cout << process_name << " killed 2" << std::endl;
		} else if (kill_proc3(notepad_pid)){
			std::cout << process_name << " killed 3" << std::endl;
		} else if (kill_proc4(notepad_pid)){
			std::cout << process_name << " killed 4" << std::endl;
		} else if (kill_proc5(notepad_pid)){
			std::cout << process_name << " killed 5" << std::endl;
		} else if (kill_proc6(notepad_pid)){
			std::cout << process_name << " killed 6" << std::endl;
		} else if (kill_proc7(notepad_pid)){
			std::cout << process_name << " killed 7" << std::endl;
		}
		
		return 0;
}